# Troll

This is the troll theme for hugo.

In order to use it, you need to configure your taxonomies:

```toml
[taxonomies]
  tag = "tag"
  category = "category"
```

Also your frontmatter must now contain tag instead of tags etc. The command `sed` will be your friend. Of course you can also change the theme and search for the hard-coded strings.

## Piwik

This template features Piwik. Add the following variables to your `config.toml`:

```
[Params]
  …
  piwikurl = "piwik.domain.de"
  piwiksite = "5"
```


## Syntax Highlighting

Syntax highlighting is done via hugo integrated chroma. See documentation: https://gohugo.io/content-management/syntax-highlighting/


## Blogroll

You can have a blogroll. Add the following array to your `config.toml`:

```
[Params]
	…
  blogroll = [
              "<a href=\"http://blogroll1.de\">Link 1</a>",
              "<a href=\"http://blog.roll2.org\">Link 2</a>",
             ]
```

## Logos

Place your logos into the `static/logos` folder. The name of the file should be `category.svg`.

